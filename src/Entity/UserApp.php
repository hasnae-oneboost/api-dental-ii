<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserAppRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity(repositoryClass=UserAppRepository::class)
 * @ApiResource(normalizationContext={"groups"={"userapp:read"}},
 *     denormalizationContext={"groups"={"userapp:write"}})
 * @UniqueEntity(
 * fields={"email"},
 * message="il existe déjà ce email '{{ value }}',veuillez saisir un autre email")
 * @ApiFilter(BooleanFilter::class, properties={"deleted"})
 */
class UserApp implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("userapp:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Groups({"userapp:read", "userapp:write"})
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"userapp:read", "userapp:write"})
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     *  @ORM\Column(type="string", length=180, unique=true)
     *  @Groups({"userapp:read", "userapp:write"})
     *    * @Assert\Regex(
 *     pattern="/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/",
 *     message="invalide email")
     */
    private $email;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * 
     *  
     */
    
    private $password;
       /**
      * @Groups("userapp:write")
      * @SerializedName("password")
      *@Assert\Length(
     *      min = 6,
     *      max = 255,
     *      minMessage = "Votre mot de passe doit comporter au moins {{ limit }} caractères",
     *      maxMessage = "Votre mot de passe doit comporter au plus {{ limit }} caractères"
     * )
     */
    private $plainPassword;
    /**
     * @ORM\Column(type="boolean")
     * @Groups({"userapp:read", "userapp:write"})
     */
    private $deleted;
     /**
     * @ORM\Column(type="json")
     *
     * @Groups({"userapp:read", "userapp:write"})
     */
    private $roles = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }
    public function getUsername(): string
    {
        return (string) $this->email;
    }
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_ADMIN';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

}
