<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use App\Repository\PatientRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity(repositoryClass=PatientRepository::class)
 *  * @ApiResource(normalizationContext={"groups"={"patient:read"}},
 *     denormalizationContext={"groups"={"patient:write"}})
 * @UniqueEntity(
 * fields={"cin"},
 * message="il existe déjà ce CIN '{{ value }}',veuillez saisir un autre CIN")
 *@ApiFilter(SearchFilter::class, properties={"cabinet":"exact"}  )
 * @ApiFilter(BooleanFilter::class, properties={"deleted"})
 * 
 * 
 */
class Patient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *  @Groups("patient:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Groups({"patient:read", "patient:write" ,"rendezvous:read" , "operation:read" })
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"patient:read", "patient:write" ,"rendezvous:read" ,"operation:read"})
     */
    private $prenom;

    /**
     * @ORM\ManyToOne(targetEntity=Cabinet::class, inversedBy="patients")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"patient:read", "patient:write"})
     */
    private $cabinet;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"patient:read", "patient:write"})
     */
    private $telephone;

    /**
     * @ORM\Column(type="boolean",name="deleted")
     *  @Groups({"patient:read", "patient:write"})
     */
    private $deleted;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Groups({"patient:read", "patient:write"})
     */
    private $sexe;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     *  @Groups({"patient:read", "patient:write"})
     */
    private $cin;

    /**
     * @ORM\Column(type="string", length=255)
     *  @Groups({"patient:read", "patient:write"})
     */
    private $adresse;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"patient:read", "patient:write"})
     */
    private $datenaissance;

    /**
     * @ORM\OneToMany(targetEntity=Rendezvous::class, mappedBy="patient")
     * 
     */
    private $rendezvouses;

    /**
     * @ORM\OneToMany(targetEntity=Paiements::class, mappedBy="patient")
     */
    private $paiements;

    /**
     * @ORM\OneToMany(targetEntity=Operation::class, mappedBy="patient")
     */
    private $operations;

    public function __construct()
    {
        $this->rendezvouses = new ArrayCollection();
        $this->paiements = new ArrayCollection();
        $this->operations = new ArrayCollection();
    }

   
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    
    public function getCabinet(): ?Cabinet
    {
        return $this->cabinet;
    }

    public function setCabinet(?Cabinet $cabinet): self
    {
        $this->cabinet = $cabinet;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getCIN(): ?string
    {
        return $this->cin;
    }

    public function setCIN(string $CIN): self
    {
        $this->cin = $CIN;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getDatenaissance(): ?\DateTimeInterface
    {
        return $this->datenaissance;
    }

    public function setDatenaissance(?\DateTimeInterface $datenaissance): self
    {
        $this->datenaissance = $datenaissance;

        return $this;
    }

    /**
     * @return Collection|Rendezvous[]
     */
    public function getRendezvouses(): Collection
    {
        return $this->rendezvouses;
    }

    public function addRendezvouse(Rendezvous $rendezvouse): self
    {
        if (!$this->rendezvouses->contains($rendezvouse)) {
            $this->rendezvouses[] = $rendezvouse;
            $rendezvouse->setPatient($this);
        }

        return $this;
    }

    public function removeRendezvouse(Rendezvous $rendezvouse): self
    {
        if ($this->rendezvouses->removeElement($rendezvouse)) {
            // set the owning side to null (unless already changed)
            if ($rendezvouse->getPatient() === $this) {
                $rendezvouse->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Paiements[]
     */
    public function getPaiements(): Collection
    {
        return $this->paiements;
    }

    public function addPaiement(Paiements $paiement): self
    {
        if (!$this->paiements->contains($paiement)) {
            $this->paiements[] = $paiement;
            $paiement->setPatient($this);
        }

        return $this;
    }

    public function removePaiement(Paiements $paiement): self
    {
        if ($this->paiements->removeElement($paiement)) {
            // set the owning side to null (unless already changed)
            if ($paiement->getPatient() === $this) {
                $paiement->setPatient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Operation[]
     */
    public function getOperations(): Collection
    {
        return $this->operations;
    }

    public function addOperation(Operation $operation): self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations[] = $operation;
            $operation->setPatient($this);
        }

        return $this;
    }

    public function removeOperation(Operation $operation): self
    {
        if ($this->operations->removeElement($operation)) {
            // set the owning side to null (unless already changed)
            if ($operation->getPatient() === $this) {
                $operation->setPatient(null);
            }
        }

        return $this;
    }

  
}
