<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210908231757 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE paiements (id INT AUTO_INCREMENT NOT NULL, patient_id INT NOT NULL, date DATE NOT NULL, charges DOUBLE PRECISION NOT NULL, taxe INT NOT NULL, remise INT DEFAULT NULL, total DOUBLE PRECISION NOT NULL, INDEX IDX_E1B02E126B899279 (patient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE paiements ADD CONSTRAINT FK_E1B02E126B899279 FOREIGN KEY (patient_id) REFERENCES patient (id)');
        $this->addSql('ALTER TABLE user_app ADD roles JSON NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE paiements');
        $this->addSql('ALTER TABLE user_app DROP roles');
    }
}
