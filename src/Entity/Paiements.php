<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PaiementsRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PaiementsRepository::class)
 * @ApiResource(normalizationContext={"groups"={"paiement:read"}},
 *     denormalizationContext={"groups"={"paiement:write"}}
 * )

 */
class Paiements
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * 
     * @Groups("paiement:read")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="float")
     * @Groups({"paiement:read", "paiement:write"})
     */
    private $charges;

    /**
     * @ORM\Column(type="integer")
     *  @Groups({"paiement:read", "paiement:write"})
     */
    private $taxe;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"paiement:read", "paiement:write"})
     */
    private $Remise;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="paiements")
     * @ORM\JoinColumn(nullable=false)
     */
    private $patient;

    /**
     * @ORM\Column(type="float")
     * @Groups({"paiement:read", "paiement:write"})
     */
    private $total;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCharges(): ?float
    {
        return $this->charges;
    }

    public function setCharges(float $charges): self
    {
        $this->charges = $charges;

        return $this;
    }

    public function getTaxe(): ?int
    {
        return $this->taxe;
    }

    public function setTaxe(int $taxe): self
    {
        $this->taxe = $taxe;

        return $this;
    }

    public function getRemise(): ?int
    {
        return $this->Remise;
    }

    public function setRemise(?int $Remise): self
    {
        $this->Remise = $Remise;

        return $this;
    }

    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

    public function getTotal(): ?float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }
}
