<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use App\Repository\RendezvousRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;


/**
 * @ORM\Entity(repositoryClass=RendezvousRepository::class)
 * @ApiResource(normalizationContext={"groups"={"rendezvous:read"}},
 *     denormalizationContext={"groups"={"rendezvous:write"}})
 * @ApiFilter(BooleanFilter::class, properties={"deleted"})
 * @ApiFilter(SearchFilter::class, properties={"cabinet":"exact"}  )
 * 
 * @ApiFilter(SearchFilter::class, properties={"patient":"exact"}  )
 *  findAll()
 */
class Rendezvous
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("rendezvous:read")
     *
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="rendezvouses")
   
     * @Groups({"rendezvous:read", "rendezvous:write"})
     */
    private $patient;

    /**
     * @ORM\ManyToOne(targetEntity=Cabinet::class, inversedBy="rendezvouses")
     * @Groups({"rendezvous:read", "rendezvous:write"})
   
     */
    private $cabinet;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"rendezvous:read", "rendezvous:write"})
     */
    private $status;

    /**
     * @ORM\Column(type="boolean",name="deleted")
     *  @Groups({"rendezvous:read", "rendezvous:write"})
     */
    private $deleted;

    

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"rendezvous:read", "rendezvous:write"})
     */
    private $datefinrdv;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"rendezvous:read", "rendezvous:write"})
     * 
     */
    private $datedebutrdv;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    public function getId(): ?int
    {
        return $this->id;
    }

   

    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

    public function getCabinet(): ?Cabinet
    {
        return $this->cabinet;
    }

    public function setCabinet(?Cabinet $Cabinet): self
    {
        $this->cabinet = $Cabinet;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getDateDebutRdv(): ?string
    {
        return $this->datedebutrdv;
    }

    public function setDateDebutRdv(string $datedebutrdv): self
    {
        $this->datedebutrdv = $datedebutrdv;

        return $this;
    }

    public function getDateFinRdv(): ?string
    {
        return $this->datefinrdv;
    }

    public function setDateFinRdv(string $datefinrdv): self
    {
        $this->datefinrdv = $datefinrdv;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
