<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210915110744 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation ADD typesoins_id INT NOT NULL');
        $this->addSql('ALTER TABLE operation ADD CONSTRAINT FK_1981A66D8F197F70 FOREIGN KEY (typesoins_id) REFERENCES types_soins (id)');
        $this->addSql('CREATE INDEX IDX_1981A66D8F197F70 ON operation (typesoins_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation DROP FOREIGN KEY FK_1981A66D8F197F70');
        $this->addSql('DROP INDEX IDX_1981A66D8F197F70 ON operation');
        $this->addSql('ALTER TABLE operation DROP typesoins_id');
    }
}
