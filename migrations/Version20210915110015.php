<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210915110015 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation DROP FOREIGN KEY FK_1981A66D1C1716D6');
        $this->addSql('DROP INDEX IDX_1981A66D1C1716D6 ON operation');
        $this->addSql('ALTER TABLE operation DROP types_soins_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation ADD types_soins_id INT NOT NULL');
        $this->addSql('ALTER TABLE operation ADD CONSTRAINT FK_1981A66D1C1716D6 FOREIGN KEY (types_soins_id) REFERENCES types_soins (id)');
        $this->addSql('CREATE INDEX IDX_1981A66D1C1716D6 ON operation (types_soins_id)');
    }
}
