<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\TypesSoinsRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=TypesSoinsRepository::class)
 *  @ApiResource(normalizationContext={"groups"={"typesoins:read"}},
 *     denormalizationContext={"groups"={"typesoins:write"}}
 * )
 *  @UniqueEntity(
 * fields={"type"},
 * message="il existe déjà ce type '{{ value }}',veuillez saisir un autre type")
 */
class TypesSoins
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("typesoins:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255 ,unique=true)
     * @Groups({"typesoins:read", "typesoins:write" ,"operation:read"})
     * 
     */
    private $type;

    /**
     * @ORM\Column(type="boolean",name="deleted")
    
     * @Groups({"typesoins:read", "typesoins:write"})

     */
    private $deleted;

    /**
     * @ORM\ManyToOne(targetEntity=Cabinet::class, inversedBy="typesoins")
     *  @Groups("typesoins:read")
     * 
     *
     */
    private $cabinet;

    /**
     * @ORM\OneToMany(targetEntity=Operation::class, mappedBy="typesoins")
     */
    private $operations;

    public function __construct()
    {
        $this->operations = new ArrayCollection();
    }

    

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getCabinet(): ?Cabinet
    {
        return $this->cabinet;
    }

    public function setCabinet(?Cabinet $cabinet): self
    {
        $this->cabinet = $cabinet;

        return $this;
    }

    /**
     * @return Collection|Operation[]
     */
    public function getOperations(): Collection
    {
        return $this->operations;
    }

    public function addOperation(Operation $operation): self
    {
        if (!$this->operations->contains($operation)) {
            $this->operations[] = $operation;
            $operation->setTypesoins($this);
        }

        return $this;
    }

    public function removeOperation(Operation $operation): self
    {
        if ($this->operations->removeElement($operation)) {
            // set the owning side to null (unless already changed)
            if ($operation->getTypesoins() === $this) {
                $operation->setTypesoins(null);
            }
        }

        return $this;
    }

    
}
