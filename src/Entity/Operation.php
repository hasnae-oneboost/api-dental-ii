<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\OperationRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=OperationRepository::class)
 * @ApiResource(normalizationContext={"groups"={"operation:read"}},
 *     denormalizationContext={"groups"={"operation:write"}}
 * )
 */
class Operation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("operation:read")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Patient::class, inversedBy="operations")
     * @ORM\JoinColumn(nullable=false)
     *  @Groups({"operation:read","operation:write"})
     */
    private $patient;

   

    /**
     * @ORM\ManyToOne(targetEntity=Cabinet::class, inversedBy="operations")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"operation:read","operation:write"})
     */
    private $cabinet;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"operation:read","operation:write"})
     */
    private $dent;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"operation:read","operation:write"})
     */
    private $deleted;

    /**
     * @ORM\ManyToOne(targetEntity=TypesSoins::class, inversedBy="operations")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"operation:read","operation:write"})
     */
    private $typesoins;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPatient(): ?Patient
    {
        return $this->patient;
    }

    public function setPatient(?Patient $patient): self
    {
        $this->patient = $patient;

        return $this;
    }

   public function getCabinet(): ?Cabinet
    {
        return $this->cabinet;
    }

    public function setCabinet(?Cabinet $cabinet): self
    {
        $this->cabinet = $cabinet;

        return $this;
    }

    public function getDent(): ?string
    {
        return $this->dent;
    }

    public function setDent(string $dent): self
    {
        $this->dent = $dent;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getTypesoins(): ?TypesSoins
    {
        return $this->typesoins;
    }

    public function setTypesoins(?TypesSoins $typesoins): self
    {
        $this->typesoins = $typesoins;

        return $this;
    }
}
