<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210914100556 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE operation (id INT AUTO_INCREMENT NOT NULL, patient_id INT NOT NULL, types_soins_id INT NOT NULL, cabinet_id INT NOT NULL, dent VARCHAR(255) NOT NULL, deleted TINYINT(1) NOT NULL, INDEX IDX_1981A66D6B899279 (patient_id), INDEX IDX_1981A66D1C1716D6 (types_soins_id), INDEX IDX_1981A66DD351EC (cabinet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE types_soins (id INT AUTO_INCREMENT NOT NULL, cabinet_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, deleted TINYINT(1) NOT NULL, INDEX IDX_CF93C81BD351EC (cabinet_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE operation ADD CONSTRAINT FK_1981A66D6B899279 FOREIGN KEY (patient_id) REFERENCES patient (id)');
        $this->addSql('ALTER TABLE operation ADD CONSTRAINT FK_1981A66D1C1716D6 FOREIGN KEY (types_soins_id) REFERENCES types_soins (id)');
        $this->addSql('ALTER TABLE operation ADD CONSTRAINT FK_1981A66DD351EC FOREIGN KEY (cabinet_id) REFERENCES cabinet (id)');
        $this->addSql('ALTER TABLE types_soins ADD CONSTRAINT FK_CF93C81BD351EC FOREIGN KEY (cabinet_id) REFERENCES cabinet (id)');
        $this->addSql('ALTER TABLE rendezvous ADD description VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE operation DROP FOREIGN KEY FK_1981A66D1C1716D6');
        $this->addSql('DROP TABLE operation');
        $this->addSql('DROP TABLE types_soins');
        $this->addSql('ALTER TABLE rendezvous DROP description');
    }
}
