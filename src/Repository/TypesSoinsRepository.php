<?php

namespace App\Repository;

use App\Entity\TypesSoins;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TypesSoins|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypesSoins|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypesSoins[]    findAll()
 * @method TypesSoins[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypesSoinsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypesSoins::class);
    }

    // /**
    //  * @return TypesSoins[] Returns an array of TypesSoins objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypesSoins
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
